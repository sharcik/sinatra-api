
class Equation
  include ActiveModel::Validations
  
  attr_accessor :type, :equation_text
  
  validates_presence_of :type, :equation_text
  validates_length_of :equation_text, :maximum => 40
  validates_length_of :equation_text, :minimum => 3
  validate :check_format
    
  def initialize(attributes)
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  clean_equation
  end
  
  private
  
  def clean_equation
    @equation_text.gsub!(/\s+/, '')
  end
  
  def check_format
    symbols_scan = @equation_text.scan(/[(\)|\*|\()]/).empty?
    scan = @equation_text.scan(/\^/).empty?
    if @type == 'linear' && symbols_scan
      self.errors.add_to_base.add_to_base(:main, "not correct type") if !scan
    end
    if @type == 'quadratic' && symbols_scan
      self.errors.add_to_base(:main, "not correct type") if scan
    end 
  end
end