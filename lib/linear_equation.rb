class LinearEquation
  
  def initialize(equation)
    data = LinearEquationParser.new
    data = data.parse(equation)
    @left_coeffts  = data[:left_coeffts]
    @right_coeffts = data[:right_coeffts]
    @left_const    = data[:left_const]
    @right_const   = data[:right_const]
  end
  
  
  def calculate
    result = sum(const) / sum(coeffts)
    "X = #{result}"
  end
  
  private
  
  def coeffts
    check_sum(transfer_coeffts_left,@left_coeffts)
  end
  
  def const
    check_sum(transfer_const_right,@right_const)
  end
  
  def transfer_const_right
    @left_const.map { |x| -x.to_f } if @left_const
  end
  
  def transfer_coeffts_left
    @right_coeffts.map { |x| -x.to_f } if @right_coeffts
  end
  
  def sum(arr)
    arr.inject(0){|sum,x| sum + x.to_f } if arr
  end
  
  def check_sum(first,second)
    if first && second
      second + first
    elsif first
      first
    else
      second
    end
  end
end