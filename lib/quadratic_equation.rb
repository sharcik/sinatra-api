class QuadraticEquation < LinearEquation

  def initialize(equation)
    data = QuadraticEquationParser.new
    data = data.parse(equation)
    @left_coeffts  = data[:left_coeffts]
    @right_coeffts = data[:right_coeffts]
    @left_const    = data[:left_const]
    @right_const   = data[:right_const]
    @left_quadra_coeffts  = data[:left_quadra_coeffts]
    @right_quadra_coeffts = data[:right_quadra_coeffts]
  end
  
  def calculate
    if descriminant > 0
      x1 = (-b + Math.sqrt(descriminant)) / (2 * a)
      x2 = (-b - Math.sqrt(descriminant)) / (2 * a)
      "Roots: #{x1}, #{x2}"
    elsif descriminant == 0
      x1 = (-b + Math.sqrt(descriminant)) / (2 * a)
      "Only one root: #{x1}"
    else
      "Not solution"
    end
  end
  
  private
  
  def descriminant
    b * b - 4 * a * c
  end
  
  def transfer_const_left
    @right_const.map { |x| -x.to_f } if @left_const
  end
  
  def transfer_quadra_coeffts_left
    @right_quadra_coeffts.map { |x| -x.to_f } if @right_quadra_coeffts
  end
  
  def transfer_quadra_const_left
    @right_const.map { |x| -x.to_f } if @right_const
  end

  def a 
    check_sum(sum(@left_quadra_coeffts),sum(transfer_quadra_coeffts_left))
  end
  
  def b
    check_sum(sum(@left_coeffts),sum(transfer_coeffts_left))
  end
  
  def c
    check_sum(sum(@left_const),sum(transfer_quadra_const_left))
  end

end