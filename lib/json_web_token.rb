class JsonWebToken

  def self.encode(payload)
    JWT.encode(payload, 'mysecretkey')
  end

  def self.decode(token)
    return HashWithIndifferentAccess.new(JWT.decode(token, 'mysecretkey')[0])
  rescue
    nil
  end
end
