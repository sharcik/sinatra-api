class QuadraticEquationParser < LinearEquationParser
  QUADRATIC_VARIABLE = /[-+]\d*[.]?\d+[A-Za-z][\^][2]/
  
  @left_quadra_coeffts  = []
  @right_quadra_coeffts = []
  
  
  def parse(string)
    # Разбиваем уравнение на левую и праву часть
    left_half  = string.split('=')[0]
    right_half = string.split('=')[1]
        
    # Добавляем недостающие знаки
    left_half  = add_first_sign(left_half)
    right_half = add_first_sign(right_half) 
    
    # Добавлем недостающие коэффициенты к обычным переменных
    left_half  = add_coeffts(left_half)
    right_half = add_coeffts(right_half)

    left_half  = add_quadra_coeffts(left_half)
    right_half = add_quadra_coeffts(right_half)

    # Получаем коэффициенты квадратных переменных
    @left_quadra_coeffts  = get_quadra_coeffts(left_half)
    @right_quadra_coeffts = get_quadra_coeffts(right_half)
    
    # Получаем коэффициенты переменных с 2 сторон
    @left_coeffts  = get_coeffts(left_half)
    @right_coeffts = get_coeffts(right_half)

    # Получаем константы с 2 сторон
    @left_const  = get_const(left_half)
    @right_const  = get_const(right_half)  
    
    {
      left_coeffts: @left_coeffts.flatten!, 
      right_coeffts: @right_coeffts.flatten!, 
      left_const: @left_const,
      right_const: @right_const,
      left_quadra_coeffts: @left_quadra_coeffts.flatten!,
      right_quadra_coeffts: @right_quadra_coeffts.flatten!
    }
  end
  
  
  private
  
  def get_quadra_coeffts(expression)
    arr = []
    expression.scan(QUADRATIC_VARIABLE).each do |member|
      arr << member.scan(COEFFT)
    end 
    expression = expression.gsub!(QUADRATIC_VARIABLE, '') if !expression.scan(QUADRATIC_VARIABLE).empty?
    arr    
  end
  
  def add_quadra_coeffts(string)
    string.gsub(/[+=-][A-Za-z][\^][2]/){ |v| v[0].to_s + '1' + v[1..-1].to_s } 
  end
end