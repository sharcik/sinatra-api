class LinearEquationParser
  OPERATOR  = /[+=-]/
  LETTERS   = /[A-Za-z]/
  COEFFT    = /[-+]\d*[.]?\d+/
  VARIABLE  = /[-+]\d*[.]?\d+[A-Za-z]/
  
  
  @right_coeffts = []
  @left_coeffts  = []
  @right_const   = []
  @left_const    = []
    
  
  def parse(string)
    
    
    # Разбиваем уравнение на левую и праву часть
    left_half  = string.split('=')[0]
    right_half = string.split('=')[1]
        
    # Добавляем недостающие знаки
    left_half  = add_first_sign(left_half)
    right_half = add_first_sign(right_half)
  
    # Добавлем недостающие коэффициенты
    left_half  = add_coeffts(left_half)
    right_half = add_coeffts(right_half)

    # Получаем коэффициенты переменных с 2 сторон
    @left_coeffts  = get_coeffts(left_half)
    @right_coeffts = get_coeffts(right_half)

    # Получаем константы с 2 сторон
    @left_const  = get_const(left_half)
    @right_const  = get_const(right_half)   
    
    {
      left_coeffts: @left_coeffts.flatten!, 
      right_coeffts: @right_coeffts.flatten!, 
      left_const: @left_const,
      right_const: @right_const
    }
  end

  private
    
  def add_first_sign(expression)
    if expression.split("")[0] != '-'
      expression.split("").unshift('+').join 
    else
      expression
    end
  end
  
  def add_coeffts(string)
    string.gsub(/[+=-][A-Za-z]/){ |v| v[0].to_s + '1' + v[-1].to_s } 
  end
    
  def get_coeffts(expression)
    arr = []
    expression.scan(VARIABLE).each do |member|
      arr << member.scan(COEFFT)
    end 
    arr
  end
    
  def get_const(expression)
    arr = []
    expression = expression.gsub!(VARIABLE, '') if !expression.scan(VARIABLE).empty?
    expression.scan(COEFFT).each { |member| arr << member } if expression
    arr
  end
  
end