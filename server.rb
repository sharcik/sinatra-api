require 'sinatra'
require 'sinatra/reloader' if development?
require 'require_all'
require 'sinatra/activerecord'
require 'jwt'

set :database, {adapter: "sqlite3", database: "database.sqlite3"}

require_all 'lib'
require_all 'models'





post '/login' do
  data = JSON.parse(request.body.read)
  username = data['username']
  password = data['password']
  
  user = User.find_by(email: username) if User.find_by(email: username)

  if user and user.authenticate(password)
    content_type :json
    get_token(user, "You log in!").to_json
  else
    halt 401
  end
  
end

post '/registration' do
  data = JSON.parse(request.body.read)
  username = data['username']
  password = data['password']
  password_confirmation = data['password_confirmation']

  user = User.new(email: username, password: password, password_confirmation: password_confirmation)

  if user.valid?
    user.save
    
    get_token(user,"Successful registration").to_json
  else

    halt 401
  end
  
end




post '/solve' do
  authenticate_request!
  
  data = JSON.parse(request.body.read)
  equation = Equation.new(type: data['type'], equation_text: data['equation_text'])

  if equation.valid?
    if equation.type == 'linear'
      equation = LinearEquation.new(data['equation_text'])
      response = equation.calculate
    else
      equation = QuadraticEquation.new(data['equation_text'])
      response = equation.calculate
    end
    response.to_json
  else
    { errors: equation.errors.full_messages.to_sentence }.to_json
  end
end







private

def authenticate_request!
  unless http_token
    halt 401
  end
  User.find(auth_token[:user_id])
rescue JWT::VerificationError, JWT::DecodeError
  halt 401
end

def auth_token
  @auth_token ||= JsonWebToken.decode(http_token)
end

def http_token
  @http_token ||= if request.env['HTTP_AUTHORIZATION'].present?
    request.env['HTTP_AUTHORIZATION'].split(' ').last
  end
end
  

def get_token(user,message)
  {
    token: JsonWebToken.encode({user_id: user.id}),
    message: message
  }
end

